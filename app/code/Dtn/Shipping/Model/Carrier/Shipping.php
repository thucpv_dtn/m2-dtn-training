<?php

namespace Dtn\Shipping\Model\Carrier;

use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;

/**
 * Class Shipping
 * @package Dtn\Shipping\Model\Carrier
 */
class Shipping extends AbstractCarrier implements CarrierInterface
{


    protected $_code = 'dtn_shipping';
    protected $_rateResultFactory;

    protected $_rateMethodFactory;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        array $data = []
    )
    {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return float
     */
    private function getShippingPrice()
    {
        return $this->getConfigData('price');;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * @param RateRequest $request
     * @return bool|\Magento\Framework\DataObject|\Magento\Shipping\Model\Rate\Result|null
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
        $method = $this->_rateMethodFactory->create();

        $freeShipTotal = $this->getConfigData('free_shipping_amount');
        $totalAmount = $request->getPackageValueWithDiscount();
        $title = $this->getConfigData('title');
        $amount = $this->getShippingPrice();

        if ($totalAmount >= $freeShipTotal) {
            $amount = 0;
        } else {
            $spendAmount = $freeShipTotal - $totalAmount;
            $title = $title . "(Spend {$spendAmount} more to get free shipping)";
        }

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($title);

        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));


        $method->setPrice($amount);
        $method->setCost($amount);

        $result->append($method);

        return $result;
    }
}