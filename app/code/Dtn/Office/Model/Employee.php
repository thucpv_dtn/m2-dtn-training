<?php

namespace Dtn\Office\Model;

use \Magento\Framework\Model\AbstractModel;

/**
 * Class Employee
 * @package Dtn\Office\Model
 */
class Employee extends AbstractModel
{

    const ENTITY = 'dtn_employee';

    public function _construct()
    {
        /* full resource classname */
        $this->_init('Dtn\Office\Model\ResourceModel\Employee');
    }
    protected function _getResource()
    {
        return parent::_getResource();
    }

    public function loadByEmail($email){
        $this->setData($this->_getResource()->loadByEmail($email));
        return $this;
    }

}