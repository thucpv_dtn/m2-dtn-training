<?php

namespace Dtn\Office\Model;

use \Magento\Framework\Model\AbstractModel;

/**
 * Class Department
 * @package Dtn\Office\Model
 */
class Department extends AbstractModel
{

    public function _construct()
    {
        /* full resource classname */
        $this->_init('Dtn\Office\Model\ResourceModel\Department');
    }

}