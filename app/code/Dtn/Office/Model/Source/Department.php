<?php

namespace Dtn\Office\Model\Source;

use Dtn\Office\Model\ResourceModel\Department\CollectionFactory;

/**
 * Class Department
 * @package Dtn\Office\Model\Source
 */
class Department
{
    protected $_departmentCollectionFactory;

    /**
     * Department constructor.
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->_departmentCollectionFactory = $collectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray(){
        $options  = [];
        $departmentCollection = $this->_departmentCollectionFactory->create();
        foreach ($departmentCollection as $department){
            $options[] = [
                'value' => $department->getId(), 'label' => $department->getName()
            ];
        }
        return $options;
    }

    /**
     * @return array
     */
    public function toArray(){
        $options  = [];
        $departmentCollection = $this->_departmentCollectionFactory->create();
        foreach ($departmentCollection as $department){
            $options[$department->getId()] = $department->getName();
        }
        return $options;
    }
}