<?php

namespace Dtn\Office\Model\ResourceModel;

use Magento\Eav\Model\Entity\AbstractEntity;

/**
 * Class Employee
 * @package Dtn\Office\Model\ResourceModel
 */
class Employee extends AbstractEntity
{
    protected $_read;
    protected $_write;

    protected function _construct()
    {
        $this->_read = 'dtn_employee_read';
        $this->_write = 'dtn_employee_write';
    }

    public function getEntityType()
    {
        if (empty($this->_type)) {
            $this->setType(\Dtn\Office\Model\Employee::ENTITY);
        }

        return parent::getEntityType();
    }
    public function loadByEmail($email){
        $connection = $this->getConnection();

        $select = $connection->select()->from($this->getEntityTable())->where('email = :email');

        $bind = [':email' => (string)$email];

        return $connection->fetchRow($select, $bind);
    }

}