<?php

namespace Dtn\Office\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Department
 * @package Dtn\Office\Model\ResourceModel
 */
class Department extends AbstractDb
{
    protected function _construct()
    {
        $this->_init("dtn_department","entity_id");
    }
}