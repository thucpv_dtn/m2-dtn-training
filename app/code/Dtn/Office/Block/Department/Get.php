<?php
namespace Dtn\Office\Block\Department;

use Dtn\Office\Model\DepartmentFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Request\Http as Request;

class Get extends Template
{
    protected $_departmentFactory;
    protected $_request;
    public function __construct(Template\Context $context, array $data = [],DepartmentFactory $departmentFactory,Request $request)
    {
        $this->_departmentFactory = $departmentFactory;
        $this->_request = $request;
        parent::__construct($context, $data);
    }
    public function getDepartment(){
        $department = $this->_departmentFactory->create();
        $params = $this->_request->getParams();
        if (isset($params['id'])){
            $department->load($params['id']);
        }
        return $department;
    }


}