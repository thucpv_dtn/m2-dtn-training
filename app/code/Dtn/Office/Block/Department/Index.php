<?php
namespace Dtn\Office\Block\Department;

use Dtn\Office\Model\ResourceModel\Department\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Request\Http as Request;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_departmentCollectionFactory;
    protected $_request;
    public function __construct(Template\Context $context, array $data = [],CollectionFactory $departmentCollectionFactory,Request $request)
    {
        $this->_departmentCollectionFactory = $departmentCollectionFactory;
        $this->_request = $request;
        parent::__construct($context, $data);
    }
    public function getDepartmentCollection(){
        $departmentCollection = $this->_departmentCollectionFactory->create();
        $params = $this->_request->getParams();
        if (isset($params['q'])){
            $filterValue = $this->_request->getParam('q');
            $departmentCollection->addFieldToFilter('name',['like' => "{$filterValue}%"]);
        }
        if (isset($params['range_id_from'])){
            $from = (int)$params['range_id_from'];
            if ($from > 0){
                $departmentCollection->addFieldToFilter('entity_id',['gteq' => "{$from}%"]);
            }
        }
        if (isset($params['range_id_to'])){
            $to = (int)$params['range_id_to'];
            if ($to > 0){
                $departmentCollection->addFieldToFilter('entity_id',['lteq' => "{$to}%"]);
            }
        }

        return $departmentCollection;
    }

    public function getSearchName(){
        $params = $this->_request->getParams();
        if (isset($params['q'])){
            return $params['q'];
        }
        return '';
    }
    public function getRangeIdFrom(){
        $params = $this->_request->getParams();
        if (isset($params['range_id_from'])){
            $from = (int)$params['range_id_from'];
            if ($from > 0){
                return $from;
            }

        }
        return '';
    }
    public function getRangeIdTo(){
        $params = $this->_request->getParams();
        if (isset($params['range_id_to'])){
            $to = (int)$params['range_id_to'];
            if ($to > 0){
                return $to;
            }
        }
        return '';
    }
}