<?php

namespace Dtn\Office\Block\Adminhtml;

class Employee extends \Magento\Backend\Block\Widget\Grid\Container
{

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    protected function _construct()
    {

        $this->_controller = 'adminhtml_employee';
        $this->_blockGroup = 'Dtn_Office';
        $this->_headerText = __('Department');
        $this->_addButtonLabel = __('Create New Employee');
        parent::_construct();
    }
}