<?php

namespace Dtn\Office\Block\Adminhtml\Employee;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Dtn\Office\Model\ResourceModel\Employee\CollectionFactory;
use Magento\Backend\Block\Widget\Grid\Extended;
use Dtn\Office\Model\Source\Department;

class Grid extends Extended
{

    protected $_collectionFactory;
    protected $_department;
    /**
     * Grid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param CollectionFactory $collectionFactory
     * @param Department $department
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CollectionFactory $collectionFactory,
        Department $department,
        array $data = []
    )
    {
        $this->_department = $department;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('employeeGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
    }

    /**
     * Prepare collection
     *
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $collection = $this->_collectionFactory->create();
        $collection->addAttributeToSelect(['dob','working_years','salary','note']);
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'index' => 'entity_id',
                'align' => 'center'
            ]
        );

        $this->addColumn(
            'department_id',
            [
                'header' => __('Department'),
                'index' => 'department_id',
                'type'=>  'options',
                'options' => $this->_department->toArray()
            ]
        );
        $this->addColumn(
            'email',
            [
                'header' => __('email'),
                'index' => 'email',
            ]
        );
        $this->addColumn(
            'firstname',
            [
                'header' => __('firstname'),
                'index' => 'firstname'
            ]
        );
        $this->addColumn(
            'lastname',
            [
                'header' => __('lastname'),
                'index' => 'lastname'
            ]
        );
        $this->addColumn(
            'working_years',
            [
                'header' => __('working_years'),
                'index' => 'working_years',
                'type'=>  'number'
            ]
        );
        $this->addColumn(
            'dob',
            [
                'header' => __('dob'),
                'index' => 'dob',
                'type'=>  'date'
            ]
        );
        $this->addColumn(
            'salary',
            [
                'header' => __('salary'),
                'index' => 'salary',
                'type'=>  'float'
            ]
        );
        $this->addColumn(
            'note',
            [
                'header' => __('note'),
                'index' => 'note'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $item
     * @return string
     */
    public function getRowUrl($item)
    {
        return $this->getUrl('*/*/edit', ['id' => $item->getId()]);
    }
}