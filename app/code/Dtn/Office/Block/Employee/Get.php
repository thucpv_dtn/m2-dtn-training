<?php
namespace Dtn\Office\Block\Employee;

use Dtn\Office\Model\EmployeeFactory;
use Dtn\Office\Model\ResourceModel\Department\CollectionFactory as DepartmentCollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Request\Http as Request;

class Get extends Template
{
    protected $_departmentCollectionFactory;
    protected $_employeeFactory;
    protected $_request;

    public function __construct(Template\Context $context, array $data = [],EmployeeFactory $employeeFactory,DepartmentCollectionFactory $departmentCollectionFactory,Request $request)
    {
        $this->_departmentCollectionFactory = $departmentCollectionFactory;
        $this->_employeeFactory = $employeeFactory;
        $this->_request = $request;
        parent::__construct($context, $data);
    }
    public function getEmployee(){
        $employee = $this->_employeeFactory->create();
        $params = $this->_request->getParams();
        if (isset($params['id'])){
            $employee->load($params['id']);
        }
        return $employee;
    }
    public function getDepartmentCollection(){
        $departmentCollection = $this->_departmentCollectionFactory->create();
        return $departmentCollection;
    }
    public function getDepartments(){
        $departments = $this->getDepartmentCollection();
        $departmentsData = [];
        foreach ($departments as $department){
            $departmentsData[$department->getId()] = $department->getName();
        }
        return $departmentsData;
    }

}