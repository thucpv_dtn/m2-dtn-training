<?php
namespace Dtn\Office\Block\Employee;

use Dtn\Office\Model\ResourceModel\Department\CollectionFactory as DepartmentCollectionFactory;
use Dtn\Office\Model\ResourceModel\Employee\CollectionFactory as EmployeeCollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Request\Http as Request;

class Index extends Template
{
    protected $_departmentCollectionFactory;
    protected $_employeeCollectionFactory;
    protected $_request;
    public function __construct(Template\Context $context, array $data = [],DepartmentCollectionFactory $departmentCollectionFactory,EmployeeCollectionFactory $employeeCollectionFactory,Request $request)
    {
        $this->_departmentCollectionFactory = $departmentCollectionFactory;
        $this->_employeeCollectionFactory = $employeeCollectionFactory;
        $this->_request = $request;
        parent::__construct($context, $data);
    }
    public function getDepartmentCollection(){
        $departmentCollection = $this->_departmentCollectionFactory->create();
        return $departmentCollection;
    }
    public function getEmployeeCollection(){
        $dateField = [
            'created_at'
        ];
        $dayField = [
            'dob_day'
        ];
        $employeeCollection = $this->_employeeCollectionFactory->create();
        $employeeCollection->addAttributeToSelect(['dob','working_years','salary','note']);
        $params = $this->_request->getParams();
        foreach ($params as $key => $value){
            if (in_array($key,$dateField)){
                $dateFilter = [];
                if (isset($value['from']) && $value['from']){
                    $dateFilter['from'] = date("Y-m-d 00:00:00",strtotime($value['from']));
                }
                if (isset($value['to']) && $value['to']){
                    $dateFilter['to'] = date("Y-m-d 23:59:59",strtotime($value['to']));
                }
                if (!empty($dateFilter)){
                    $employeeCollection->addAttributeToFilter($key,$dateFilter);
                }
            }elseif (in_array($key,$dayField)){
                if ($value){
                    $dateFilter = $value;
                    if (strlen($dateFilter) < 2){
                        $dateFilter = "0" . $dateFilter;
                    }
                    $nameAttr = str_replace("_day","",$key);
                    $employeeCollection->addAttributeToFilter($nameAttr,['like' => "-{$dateFilter}%"]);
                }
            }else{
                if (isset($value['from']) && $value['from']){
                    $from = (int)$value['from'];
                    if ($from > 0){
                        $employeeCollection->addAttributeToFilter('entity_id',['gteq' => "{$from}%"]);
                    }
                }
                if (isset($value['to']) && $value['to']){
                    $to = (int)$value['to'];
                    if ($to > 0){
                        $employeeCollection->addAttributeToFilter('entity_id',['lteq' => "{$to}%"]);
                    }
                }
                if (!is_array($value)){
                    $employeeCollection->addAttributeToFilter($key,$value);
                }
            }
        }

        return $employeeCollection;
    }

    public function getParamsData(){
        $params = $this->_request->getParams();
        if (isset($params['q'])){
            return $params['q'];
        }
        return $params;
    }
    public function getDepartments(){
        $departments = $this->getDepartmentCollection();
        $departmentsData = [];
        foreach ($departments as $department){
            $departmentsData[$department->getId()] = $department->getName();
        }
        return $departmentsData;
    }
}