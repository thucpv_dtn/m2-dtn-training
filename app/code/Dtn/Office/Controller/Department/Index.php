<?php
namespace Dtn\Office\Controller\Department;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;


/**
 * Class Index
 * @package Dtn\Office\Controller\Department
 */
class Index extends Action
{
    protected $_pageFactory;
    protected $_request;
    public function __construct(
        Context $context,
       PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        return $this->_pageFactory->create();
    }
}