<?php
namespace Dtn\Office\Controller\Department;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface as Message;
use Dtn\Office\Model\DepartmentFactory;


/**
 * Class Index
 * @package Dtn\Office\Controller\Department
 */
class Create extends Action
{
    protected $_pageFactory;
    protected $_request;
    protected $_resultRedirect;
    protected $_message;
    protected $_departmentFactory;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ResultFactory $result,
        Message $message,
        DepartmentFactory $departmentFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_resultRedirect = $result;
        $this->_message= $message;
        $this->_departmentFactory = $departmentFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $department = $this->_departmentFactory->create();
        $params = $this->getRequest()->getParams();

        if ($params['name']){
            $department->setName($params['name'])->save();
            $this->messageManager->addSuccess('Department Create Success');
        }
        return $this->resultRedirectFactory->create()->setPath('dtn_office/department/index');
    }
}