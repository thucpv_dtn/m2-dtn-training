<?php
namespace Dtn\Office\Controller\Adminhtml\Department;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;


/**
 * Class Index
 * @package Dtn\Office\Controller\Department\Adminhtml
 */
class Index extends Action
{
    protected $_pageFactory;
    protected $_request;
    public function __construct(
        Context $context,
       PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_pageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Department')));
        return $resultPage;
    }
}