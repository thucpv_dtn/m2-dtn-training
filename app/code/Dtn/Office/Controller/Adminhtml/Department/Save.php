<?php

namespace Dtn\Office\Controller\Adminhtml\Department;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Dtn\Office\Model\Department;

/**
 * Class Index
 * @package Dtn\Office\Controller\Department\Adminhtml
 */
class Save extends Action
{
    protected $_model;

    public function __construct(
        Context $context,
        Department $department
    )
    {
        $this->_model = $department;
        return parent::__construct($context);
    }


    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $this->_model->load($id);
            }

            if (!isset($data['name']) || $data['name']==''){
                $this->messageManager->addError("Name required");
                return $resultRedirect->setPath('*/*/');
            }
            try {
                $this->_model->setName($data['name'])->save();
                $this->messageManager->addSuccess(__('You add Department success'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['id' => $this->_model->getId(), '_current' => true]
                    );
                }
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the store'));
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}