<?php

namespace Dtn\Office\Controller\Adminhtml\Employee;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Dtn\Office\Model\Employee;

/**
 * Class Index
 * @package Dtn\Office\Controller\Employee\Adminhtml
 */
class Save extends Action
{
    protected $_model;

    public function __construct(
        Context $context,
        Employee $employee
    )
    {
        $this->_model = $employee;
        return parent::__construct($context);
    }


    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            $id = $this->getRequest()->getParam('id');

            $email = ( (isset($data['email']) && trim($data['email']) != '') ? trim($data['email']) : false);
            if (!$email){
                $this->messageManager->addError('Email required');
                return $this->resultRedirectFactory->create()->setPath('*/*/*');
            }

            if ($id) {
                $data['entity_id'] = $id;
                $tmpModel = $this->_model;
                $this->_model->load($id);
                if ($email){
                    if ($this->_model->getEmail() != $email){
                        $tmpModel->loadByEmail($email);
                        if ($tmpModel->getId()){
                            $this->messageManager->addError('Employee Email exist');
                            return $this->resultRedirectFactory->create()->setPath('*/*/*');
                        }
                    }
                }
            }


            try {
                $this->_model->setData($data)->save();
                $this->messageManager->addSuccess(__('You add Employee success'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['id' => $this->_model->getId(), '_current' => true]
                    );
                }
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the store'));
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}