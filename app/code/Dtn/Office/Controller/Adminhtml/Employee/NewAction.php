<?php
namespace Dtn\Office\Controller\Adminhtml\Employee;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;


/**
 * Class NewAction
 * @package Dtn\Office\Controller\Adminhtml\Employee
 */
class NewAction extends Action
{
    protected $_resultForwardFactory;
    protected $_request;
    public function __construct(
        Context $context,
        ForwardFactory $resultForwardFactory
    )
    {
        $this->_resultForwardFactory = $resultForwardFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $resultForwardFactory = $this->_resultForwardFactory->create();
        return $resultForwardFactory->forward('edit');
    }
}