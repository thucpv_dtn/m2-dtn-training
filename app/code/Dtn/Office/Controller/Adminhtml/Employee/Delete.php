<?php

namespace Dtn\Office\Controller\Adminhtml\Employee;

use Magento\Backend\App\Action;


/**
 * Class Index
 * @package Dtn\Office\Controller\Department\Adminhtml
 */
class Delete extends Action
{
    public function execute()
    {

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create(\Dtn\Office\Model\Employee::class);
        $resultRedirect = $this->resultRedirectFactory->create();
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This page no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                return $resultRedirect->setPath('*/*/');
            }
        }

        try {
            $model->delete();
            $this->messageManager->addSuccess(__('You Delete Employee success'));
        } catch (Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the store'));
        }

        return $resultRedirect->setPath('*/*/');
    }
}