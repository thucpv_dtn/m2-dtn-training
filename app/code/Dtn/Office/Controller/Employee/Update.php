<?php
namespace Dtn\Office\Controller\Employee;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface as Message;
use Dtn\Office\Model\EmployeeFactory;


/**
 * Class Update
 * @package Dtn\Office\Controller\Employee
 */
class Update extends Action
{
    protected $_pageFactory;
    protected $_request;
    protected $_resultRedirect;
    protected $_message;
    protected $_employeeFactory;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ResultFactory $result,
        Message $message,
        EmployeeFactory $employeeFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_resultRedirect = $result;
        $this->_message= $message;
        $this->_employeeFactory = $employeeFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $employee = $this->_employeeFactory->create();
        $params = $this->getRequest()->getParams();
        if (isset($params['id'])){
            $employee->load($params['id']);
        }
        if (!$employee->getId()){
            $this->messageManager->addError('Employee Not exist');
            return $this->resultRedirectFactory->create()->setPath('dtn_office/employee/index');
        }

        if (isset($params['email'])){
            if ($params['email'] != $employee->getEmail()){
                $employeeEmail = $this->_employeeFactory->create();
                $employeeEmail->loadByEmail($params['email']);
                if ($employeeEmail->getId()){
                    $this->messageManager->addError('Employee Email exist');
                    return $this->resultRedirectFactory->create()->setPath('dtn_office/employee/get',['id'=>$params['id']]);
                }
            }
        }
        foreach ($params as $key => $value){
            if ($value){
                $employee->setData($key,$value);
            }
        }
        $employee->save();
        $this->messageManager->addSuccess('Employee Update Success');
        return $this->resultRedirectFactory->create()->setPath('dtn_office/employee/index');
    }
}