<?php
namespace Dtn\Office\Controller\Employee;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface as Message;
use Dtn\Office\Model\EmployeeFactory;


/**
 * Class Create
 * @package Dtn\Office\Controller\Employee
 */
class Create extends Action
{
    protected $_pageFactory;
    protected $_request;
    protected $_resultRedirect;
    protected $_message;
    protected $_employeeFactory;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ResultFactory $result,
        Message $message,
        EmployeeFactory $employeeFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_resultRedirect = $result;
        $this->_message= $message;
        $this->_employeeFactory = $employeeFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $employee = $this->_employeeFactory->create();
        $params = $this->getRequest()->getParams();
        if (isset($params['email']) && trim($params['email']) != ''){
            $employee->loadByEmail($params['email']);
            if ($employee->getId()){
                $this->messageManager->addError('Employee Email exist');
                return $this->resultRedirectFactory->create()->setPath('dtn_office/employee/add');
            }
        }else{
            $this->messageManager->addError('Email required');
            return $this->resultRedirectFactory->create()->setPath('dtn_office/employee/add');
        }
        foreach ($params as $key => $value){
            if ($value){
                $employee->setData($key,$value)->save();
            }
        }
        $employee->save();
        $this->messageManager->addSuccess('Employee Create Success');
        return $this->resultRedirectFactory->create()->setPath('dtn_office/employee/index');
    }
}