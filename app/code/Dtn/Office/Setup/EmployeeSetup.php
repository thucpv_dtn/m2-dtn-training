<?php

namespace Dtn\Office\Setup;

use Magento\Eav\Model\Entity\Setup\Context;
use Magento\Eav\Model\Entity\Setup\PropertyMapperInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;
use Magento\Eav\Model\Validator\Attribute\Code;
use Magento\Eav\Setup\AddOptionToAttribute;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Eav\Setup\EavSetup;
use \Dtn\Office\Model\EmployeeFactory;

/**
 * Class EmployeeSetup
 * @package Dtn\Office\Setup
 */
class EmployeeSetup extends EavSetup
{

    private $_employeeFactory;

    public function __construct(ModuleDataSetupInterface $setup, Context $context, CacheInterface $cache, CollectionFactory $attrGroupCollectionFactory, Code $attributeCodeValidator = null, AddOptionToAttribute $addAttributeOption = null, EmployeeFactory $employeeFactory)
    {
        $this->_employeeFactory = $employeeFactory;
        parent::__construct($setup, $context, $cache, $attrGroupCollectionFactory, $attributeCodeValidator, $addAttributeOption);
    }

    public function createEmployee($data = [])
    {
        return $this->_employeeFactory->create($data);
    }

    /**
     * @return array
     */
    public function getDefaultEntities()
    {
        $employeeEntity = \Dtn\Office\Model\Employee::ENTITY;

        return [
            $employeeEntity => [
                'entity_model' => \Dtn\Office\Model\ResourceModel\Employee::class,
                'table' => $employeeEntity . '_entity',
                'attributes' => [
                    'department_id' => [
                        'type' => 'static',
                        'label' => 'Department Id',
                        'required' => false,
                        'sort_order' => 0,
                        'visible' => false,
                    ],
                    'email' => [
                        'type' => 'static',
                        'label' => 'Working Years',
                        'required' => false,
                        'sort_order' => 0,
                        'visible' => false,
                    ],
                    'firstname' => [
                        'type' => 'static',
                        'label' => 'Working Years',
                        'required' => false,
                        'sort_order' => 0,
                        'visible' => false,
                    ],
                    'lastname' => [
                        'type' => 'static',
                        'label' => 'Working Years',
                        'required' => false,
                        'sort_order' => 0,
                        'visible' => false,
                    ],
                    'working_years' => [
                        'type' => 'int',
                        'label' => 'Working Years',
                        'required' => false,
                        'sort_order' => 0,
                        'visible' => false,
                    ],
                    'dob' => [
                        'type' => 'datetime',
                        'label' => 'dob',
                        'required' => false,
                        'sort_order' => 1,
                        'visible' => false,
                    ],
                    'salary' => [
                        'type' => 'decimal',
                        'label' => 'dob',
                        'required' => false,
                        'sort_order' => 2,
                        'visible' => false,
                    ],
                    'note' => [
                        'type' => 'text',
                        'label' => 'dob',
                        'required' => false,
                        'sort_order' => 12,
                        'visible' => false,
                    ]
                ],
            ]
        ];
    }
}