<?php

namespace Dtn\Office\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Dtn\Office\Model\DepartmentFactory;
use Dtn\Office\Model\EmployeeFactory;

/**
 * Class InstallData
 * @package Dtn\Office\Setup
 */
class UpgradeData implements UpgradeDataInterface
{

    private $_employeeModel;
    private $_departmentModel;
    private $_employeeSetup;

    public function __construct(
        DepartmentFactory $department,
        EmployeeFactory $employee
    )
    {
        $this->_employeeModel = $employee;
        $this->_departmentModel = $department;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.1', '<')) {
            $this->installSampleData();
        }
    }

    public function installSampleData()
    {
        $sampleDepartmentData = [
            ['name' => 'Marketing Department'],
            ['name' => 'Developer Department'],
            ['name' => 'Hr Department'],
            ['name' => 'Technical Department'],
            ['name' => 'Accounting Department']
        ];
        $sampleEmployeeData = [
            [
                'email' => 'amy@dtn.com.vn',
                'firstname' => 'nguyen',

                'lastname' => 'anh',
                'working_years' => '10',
                'dob' => '10/10/2010',
                'salary' => '5000',
                'note' => 'xinh dep yeu quy',
            ],
            [
                'email' => 'huyendao@dtn.com.vn',
                'firstname' => 'Dao',
                'lastname' => 'huyen',
                'working_years' => '8',
                'dob' => '10/10/2010',
                'salary' => '4000',
                'note' => 'xinh dep yeu quy',
            ],
            [
                'email' => 'khanh@dtn.com.vn',
                'firstname' => 'nguyen',
                'lastname' => 'khank',
                'working_years' => '8',
                'dob' => '10/10/2010',
                'salary' => '4000',
                'note' => 'dep trai yeu quy',
            ],
            [
                'email' => 'ngocanh@dtn.com.vn',
                'firstname' => 'nguyen',
                'lastname' => 'anh',
                'working_years' => '8',
                'dob' => '10/10/2010',
                'salary' => '4500',
                'note' => 'thay va ban',
            ],
            [
                'email' => 'phuchoa@dtn.com.vn',
                'firstname' => 'nguyen',
                'lastname' => 'hoa',
                'working_years' => '8',
                'dob' => '10/10/2010',
                'salary' => '4500',
                'note' => 'thay va ban',
            ],
        ];

        foreach ($sampleDepartmentData as $key => $data) {
            $department = $this->_departmentModel->create();
            $department->addData($data)->save();
            $departmentId = $department->getId();
            $employeeData = $sampleEmployeeData[$key];
            $employeeData['department_id'] = $departmentId;
            $employee = $this->_employeeModel->create();
            $employee->addData($employeeData)->save();
        }
    }
}