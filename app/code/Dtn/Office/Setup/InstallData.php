<?php

namespace Dtn\Office\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 * @package Dtn\Office\Setup
 */
class InstallData implements InstallDataInterface
{

    private $_employeeSetupFactory;

    public function __construct(
        \Dtn\Office\Setup\EmployeeSetupFactory $employeeSetupFactory
    )
    {
        $this->_employeeSetupFactory = $employeeSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $employeeSetup = $this->_employeeSetupFactory->create(['setup' => $setup]);
        $employeeSetup->installEntities();
        $setup->endSetup();
    }
}