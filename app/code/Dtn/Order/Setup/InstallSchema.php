<?php

namespace Dtn\Order\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Dtn\Order\Setup
 */
class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'special_request',
            [
                'type' => Table::TYPE_TEXT,
                'size' => 255,
                'length' => 255,
                'nullable' => true,
                'comment' => 'Special Request'
            ]
        );
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'special_request',
            [
                'type' => Table::TYPE_TEXT,
                'size' => 255,
                'length' => 255,
                'nullable' => true,
                'comment' => 'Special Request'
            ]
        );
        $installer->endSetup();
    }
}