<?php

namespace Dtn\Order\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\DataObject\Copy;

/**
 * Class CopyFieldsetToOrderObserver
 * @package Dtn\Order\Observer
 */
class CopyFieldsetToOrderObserver implements ObserverInterface
{
    protected $_objectCopyService;

    public function __construct(
        Copy $objectCopyService
    ) {
        $this->_objectCopyService = $objectCopyService;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getData('order');
        /* @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getData('quote');

        $this->_objectCopyService->copyFieldsetToTarget('sales_convert_quote', 'to_order', $quote, $order);
        return $this;
    }
}