<?php
namespace Dtn\Order\Plugin\Checkout\Model;
/**
 * Class ShippingInformationManagement
 * @package Dtn\Order\Plugin\Checkout\Model
 */
class ShippingInformationManagement
{

    protected $_quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->_quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getShippingAddress()->getExtensionAttributes();
        $specialRequest = $extAttributes->getSpecialRequest();
        $quote = $this->_quoteRepository->getActive($cartId);
        $quote->setSpecialRequest($specialRequest);
    }
}